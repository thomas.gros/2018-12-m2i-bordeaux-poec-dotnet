﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MusicForever.Models;
using WebEmpty.Interfaces;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MusicForever.Api
{
    [Route("api/[controller]")]
    public class SongsController : Controller
    {
        private readonly ISongRepository _songRepository;

        public SongsController(ISongRepository songRepository)
        {
            _songRepository = songRepository;
        }


        [HttpGet]
        public IEnumerable<Song> AllPersons()
        {
            return _songRepository.All();
        }

        [HttpGet("{id}")]
        public Song Find(string id)
        {
           return _songRepository.Find(id);

        }
    }
}
