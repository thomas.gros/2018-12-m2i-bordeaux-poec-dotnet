﻿using System;
using System.Collections.Generic;
using MusicForever.Models;
using WebEmpty.Models;

namespace WebEmpty.Interfaces
{
    // La notion de Repository est également un pattern
    // retrouver dans un livre qui s'appelle Domain Driven Design
    public interface ISongRepository
    {
        bool DoesSongExist(string id);
        IEnumerable<Song> All();
        Song Find(string id);
        void Add(Song article);
    }

}
