﻿using System;
namespace MusicForever.Models
{
    public class Song
    {
        public string ID { get; set; }
        public string Title { get; set; }
        public string Artist { get; set; }
        public string Album { get; set; }
        public DateTime LastTimeListened { get; set; }
        public TimeSpan Duration { get; set; }
        public string Source { get; set; }
        public string Mediatype { get; set; }
        public string Picture { get; set; }
    }
}
