﻿using System;
using System.Linq;
using System.Collections.Generic;
using WebEmpty.Interfaces;
using MusicForever.Models;

namespace WebEmpty.Models
{
    public class SongRepository: ISongRepository
    {
        private IList<Song> _songList;

        public SongRepository()
        {
            _songList = new List<Song>();

            _songList.Add(new Song
            {
                ID = Guid.NewGuid().ToString(),
                Title = "Crowd Cheering",
                Artist = "Sample Videos",
                Album = "Audio Samples",
                LastTimeListened = DateTime.Today.AddDays(-3),
                Duration = new TimeSpan(0, 3, 11),
                Source = "https://sample-videos.com/audio/mp3/crowd-cheering.mp3",
                Mediatype = "audio/mp3",
                Picture = "https://sample-videos.com/images/imgw.png"

            });

            _songList.Add(new Song
            {
                ID = Guid.NewGuid().ToString(),
                Title = "Wave",
                Artist = "Sample Videos",
                Album = "Audio Samples",
                LastTimeListened = DateTime.Today.AddDays(-6),
                Duration = new TimeSpan(0, 2, 50),
                Source = "https://sample-videos.com/audio/mp3/wave.mp3",
                Mediatype = "audio/mp3",
                Picture = "https://sample-videos.com/images/imgw.png"
            });

        }

        public IEnumerable<Song> All() 
        {
            return _songList;
        }

        public bool DoesSongExist(string id)
        {
            return _songList.Any(item => item.ID == id);
        }

        public Song Find(string id)
        {
            return _songList.FirstOrDefault(item => item.ID == id);
        }

        public void Add(Song article)
        {
            _songList.Add(article);
        }
    }
}
