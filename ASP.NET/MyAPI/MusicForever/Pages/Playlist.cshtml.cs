﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MusicForever.Models;
using WebEmpty.Interfaces;

namespace MusicForever.Pages
{
    public class PlaylistModel : PageModel
    {
        private readonly ISongRepository _songRepository;

        public PlaylistModel(ISongRepository songRepository)
        {
            _songRepository = songRepository;
        }

        public IEnumerable<Song> Songs { get; private set; }

        public void OnGet()
        {
            Songs = _songRepository.All();
        }
    }
}
