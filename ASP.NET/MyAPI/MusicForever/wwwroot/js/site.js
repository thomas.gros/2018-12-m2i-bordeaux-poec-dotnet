﻿const songs = document.querySelectorAll("#main-playlist tbody tr");

songs.forEach(s => s.addEventListener('click', clickListener));

function clickListener(e) {
    fetch(`https://localhost:5001/api/songs/${e.currentTarget.id.split('id-')[1]}`)
        .then(response => response.json())
        .then(play);
} 

function play(song) {
    // update playlist IHM
    const oldCurrent = document.querySelector(`#main-playlist tbody tr.current`);
    if(oldCurrent) {
        oldCurrent.classList.toggle('current');
    }

    const newCurrent = document.querySelector(`#id-${song.id}`);
    newCurrent.classList.toggle('current');

    // update player
    document
        .querySelector('#main-player')
        .innerHTML = 
`   <img class="player__picture" src="${song.picture}">
    <audio class="player__controls" controls>
      <source src="${song.source}" type="${song.mediatype}">
      <p>Votre navigateur ne prend pas en charge l'audio HTML. Voici un
         un <a href="${song.source}">lien vers le fichier audio</a> pour le 
         télécharger.</p>
    </audio>
`

        //`Playing: ${song}`;
}