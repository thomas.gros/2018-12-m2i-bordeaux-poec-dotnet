﻿using System;
using System.IO;
using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace MyAPI.Controllers
{
    [Route("api/hello")]
    public class HelloController : ControllerBase
    {
        public HelloController()
        {
        }

        //[HttpGet]
        //public string Hello()
        //{
        //    return "hello";
        //}

        //[HttpGet]
        //public string[] Hello()
        //{
        //    return new string[] { "hello", "world" };
        //}



        // https://stackoverflow.com/a/46802299/254439
        [HttpGet]
        public ContentResult Index()
        {
            return new ContentResult
            {
                ContentType = "text/html",
                StatusCode = (int)HttpStatusCode.OK,
                Content = "<html><body>Hello World</body></html>" // plus compliqué si on veut retourner un fichier
            };
        }
    }
}
