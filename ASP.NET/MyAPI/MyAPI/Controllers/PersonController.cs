﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MyAPI.Models;

namespace MyAPI.Controllers
{
    [Route("api/person")]
    public class PersonController
    {
        private List<Person> _personList;

        public PersonController()
        {
            _personList = new List<Person>();

            var john = new Person
            {
                ID = "1",
                Firstname = "John",
                Lastname = "Doe",
                Photo = "https://tinyfac.es/data/avatars/475605E3-69C5-4D2B-8727-61B7BB8C4699-500w.jpeg"
            };

            var sally = new Person
            {
                ID = "2",
                Firstname = "Sally",
                Lastname = "Smith",
                Photo = "https://randomuser.me/api/portraits/women/44.jpg"
            };

            _personList.Add(john);
            _personList.Add(sally);
        }


        [HttpGet]
        public List<Person> AllPersons()
        {
            return _personList;
        }

        [HttpGet("{id}")]
        public Person Find(string id)
        {
            // _personList.
           Person person = null;
           foreach(Person p in _personList)
            {
                if (p.ID == id)
                {
                    person = p;
                }
            }

            return person;

        }
    }
}
