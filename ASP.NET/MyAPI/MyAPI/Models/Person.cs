﻿using System;
namespace MyAPI.Models
{
    public class Person
    { 
        public string ID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Photo { get; set; }
    }
}
