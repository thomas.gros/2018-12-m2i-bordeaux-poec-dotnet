﻿using System;
using System.Collections.Generic;
using WebEmpty.Models;

namespace WebEmpty.Interfaces
{
    // La notion de Repository est également un pattern
    // retrouver dans un livre qui s'appelle Domain Driven Design
    public interface IArticleRepository
    {
        bool DoesArticleExist(string id);
        IEnumerable<Article> All();
        Article Find(string id);
        void Add(Article article);
    }

}
