﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebEmpty.Models
{
    public class Article
    {
        public string ID { get; set; }

        // https://docs.microsoft.com/en-us/dotnet/api/system.componentmodel.dataannotations?view=netframework-4.7.2
        [Required, StringLength(10)]
        public string Title { get; set; }

        [Required, Url]
        public string Image { get; set; }

        [Required, StringLength(100)]
        public string Description { get; set; }
    }
}
