﻿using System;
using System.Linq;
using System.Collections.Generic;
using WebEmpty.Interfaces;

namespace WebEmpty.Models
{
    public class ArticleRepository: IArticleRepository
    {
        private IList<Article> _articleList;

        public ArticleRepository()
        {
            _articleList = new List<Article>();

            _articleList.Add(new Article
            {
                ID = Guid.NewGuid().ToString(),
                Title = "Nicolas Hulot et Laurent Berger : « 66 propositions pour un pacte social et écologique ",
                Image = "https://img.lemde.fr/2019/03/04/238/0/3500/2332/650/0/75/0/2ccc4c1_Sdp4voAVq3d9gefmNpE0HlZA.jpg",
                Description = "Au nom d’une coalition inédite de 19 organisations, l’ancien ministre et le secrétaire général de la CFDT présentent un pacte visant à concilier transition environnementale et équité."
            });

            _articleList.Add(new Article
            {
                ID = Guid.NewGuid().ToString(),
                Title = "Un « trésor scientifique » pourrait révéler les secrets des Itzaes, peuple maya du Yucatan",
                Image = "https://img.lemde.fr/2019/03/04/0/0/1280/850/688/0/60/0/1efbbd0_f0f2ba5a5b40469bb794025553ab8ef5-f0f2ba5a5b40469bb794025553ab8ef5-0.jpg",
                Description = "Des centaines d’objets en céramique, dont certains datent de l’époque post-classique (700-1000 après J.-C.) ont été découverts sur le site de Chichen Itza au Mexique."
            });
        }

        public IEnumerable<Article> All() 
        {
            return _articleList;
        }

        public bool DoesArticleExist(string id)
        {
            return _articleList.Any(item => item.ID == id);
        }

        public Article Find(string id)
        {
            return _articleList.FirstOrDefault(item => item.ID == id);
        }

        public void Add(Article article)
        {
            _articleList.Add(article);
        }
    }
}
