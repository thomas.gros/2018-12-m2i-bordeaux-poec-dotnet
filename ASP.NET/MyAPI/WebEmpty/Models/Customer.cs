﻿using System;
namespace WebEmpty.Model
{
    public class Customer
    {
        public string ID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
    }
}
