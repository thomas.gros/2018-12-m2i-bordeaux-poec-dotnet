﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebEmpty.Models;

namespace WebEmpty.Pages
{
    public class ArticleModel : PageModel
    {

        public Article article;

        public ArticleModel()
        {
            article = new Article
            {
                ID = "1",
                Title = "Nicolas Hulot et Laurent Berger : « 66 propositions pour un pacte social et écologique ",
                Image = "https://img.lemde.fr/2019/03/04/238/0/3500/2332/650/0/75/0/2ccc4c1_Sdp4voAVq3d9gefmNpE0HlZA.jpg",
                Description = "Au nom d’une coalition inédite de 19 organisations, l’ancien ministre et le secrétaire général de la CFDT présentent un pacte visant à concilier transition environnementale et équité."
            };
        }

        public void OnGet()
        {
        }
    }
}
