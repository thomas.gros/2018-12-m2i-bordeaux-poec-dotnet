﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebEmpty.Interfaces;
using WebEmpty.Models;

namespace WebEmpty.Pages
{
    public class CreateArticleModel : PageModel
    {
        private readonly IArticleRepository _articleRepository;

        public CreateArticleModel(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        [BindProperty]
        public Article Article { get; set; }
         
        public void OnGet()
        {
        }

        public IActionResult OnPost()
        {
            if( ! ModelState.IsValid)
            {
                return Page(); // retourne la page courante
            }

            // rajouter l'article dans la base de données
            _articleRepository.Add(Article);

            return RedirectToPage("/ListArticle"); // redirection HTTP vers /Index
        }
    }
}
