﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebEmpty.Model;

namespace WebEmpty.Pages
{
    public class CustomerModel : PageModel
    {
        public IList<Customer> _customerList;

        public CustomerModel()
        {
            _customerList = new List<Customer>();

            var customer = new Customer
            {
                ID = "1",
                Firstname = "John",
                Lastname = "Doe"
            };

            _customerList.Add(customer);

            _customerList.Add(new Customer
            {
                ID = "2",
                Firstname = "Sally",
                Lastname = "Smith"
            });
        }

        public void OnGet()
        {
        }
    }
}