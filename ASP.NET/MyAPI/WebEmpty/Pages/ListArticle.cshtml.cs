﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using WebEmpty.Interfaces;
using WebEmpty.Models;

namespace WebEmpty.Pages
{
    public class ListArticleModel : PageModel
    {

        private readonly IArticleRepository _articleRepository;

        public ListArticleModel(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }


        public IEnumerable<Article> Articles { get; private set; }

        public void OnGet()
        {
            Articles = _articleRepository.All();
        }
    }
}
