﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

/*
 * Exercice:
 * 1) déclarer une variable de type tableau
 * contenant les données string suivantes:
 * "hello", "world", "!"
 * 
 * 2) dans la page razor, parcourir via un
 * foreach ce tableau pour générer le code
 * <ul>
 *  <li>hello</li>
 *  <li>world</li
 *  <li>!</li>
 * </ul>
 * */


namespace WebEmpty.Pages
{
    public class ModeldemoModel : PageModel
    {

        public int data;

        public string[] tab;

        public void OnGet()
        {
            data = 42;
            tab = new string[] {"hello", "world", "!"};
        }
    }
}
