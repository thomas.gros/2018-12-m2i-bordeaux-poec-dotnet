import { Person } from './person';

export const PERSONS: Person[] = [
  { id: '11', firstname: 'Mr. Nice', lastname: 'Doe', photo: '...' },
  { id: '12', firstname: 'Sally', lastname: 'Smith', photo: '...' },
];
