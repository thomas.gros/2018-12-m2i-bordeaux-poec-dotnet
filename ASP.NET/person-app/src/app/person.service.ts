import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { MessageService } from './message.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Person } from './person';
import { PERSONS } from './mock-persons';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  personsUrl = 'https://localhost:5001/api/person';
  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  getPersons(): Observable<Person[]> {
    // return of(PERSONS);
    return this.http.get<Person[]>(this.personsUrl)
    .pipe(
      tap(_ => this.log('fetched persons')),
      catchError(this.handleError('getPersons', []))
    );
  }

  getPerson(id: string): Observable<Person> {
    const url = `${this.personsUrl}/${id}`;
    return this.http.get<Person>(url).pipe(
      tap(_ => this.log(`fetched person id=${id}`)),
      catchError(this.handleError<Person>(`getPerson id=${id}`))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
private handleError<T>(operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // TODO: better job of transforming error for user consumption
    this.log(`${operation} failed: ${error.message}`);

    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
}

  private log(message: string) {
    this.messageService.add(`PersonService: ${message}`);
  }
}
