export class Person {
  id: string;
  firstname: string;
  lastname: string;
  photo: string;
}
