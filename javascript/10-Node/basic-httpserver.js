const http = require('http');

function httpRequestHandler(request, response) {
    // response.setHeader('Content-Type', 'text/plain');
    response.setHeader('Content-Type', 'text/html');
    response.end('<p>hello</p>');
} 

const server = http.createServer(httpRequestHandler);

server.listen(8080);