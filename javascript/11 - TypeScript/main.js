"use strict";
exports.__esModule = true;
var morsetranslator_1 = require("./morsetranslator");
var translator = new morsetranslator_1.MorseTranslator();
var msg_ori = "hello world";
var morse = translator.translate(msg_ori);
var msg = translator.reversetranslate(morse);
console.log(morse);
console.log(msg);
console.log("msg_ori === msg: ", msg_ori === msg);
//# sourceMappingURL=main.js.map