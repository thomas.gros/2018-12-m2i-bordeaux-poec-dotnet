import { Translator } from './translator';
import { RotTranslator } from './rottranslator';
import { MorseTranslator } from './morsetranslator';

let translator : Translator = new MorseTranslator();
let msg_ori = "hello world";
let morse = translator.translate(msg_ori);
let msg  = translator.reversetranslate(morse);

console.log(morse);
console.log(msg);
console.log("msg_ori === msg: ", msg_ori === msg)