"use strict";
exports.__esModule = true;
var MorseTranslator = (function () {
    function MorseTranslator() {
    }
    MorseTranslator.prototype.translate = function (msg) {
        return msg.split(' ')
            .map(function (w) { return w.split('').map(function (c) { return MorseTranslator.msgToMorseRef[c]; }).join(' '); })
            .join('  ');
    };
    MorseTranslator.prototype.reversetranslate = function (msg) {
        return msg.split('  ')
            .map(function (w) { return w.split(' ').map(function (c) { return MorseTranslator.morseToMsgRef[c]; }).join(''); })
            .join(' ');
    };
    MorseTranslator.msgToMorseRef = {
        'a': '.-',
        'b': '-...',
        'c': '-.-.',
        'd': '-..',
        'e': '.',
        'f': '..-.',
        'g': '--.',
        'h': '....',
        'i': '..',
        'j': '.---',
        'k': '-.-',
        'l': '.-..',
        'm': '--',
        'n': '-.',
        'o': '---',
        'p': '.--.',
        'q': '--.-',
        'r': '.-.',
        's': '...',
        't': '-',
        'u': '..-',
        'v': '...-',
        'w': '.--',
        'x': '-..-',
        'y': '-.--',
        'z': '--..',
        '1': '.----',
        '2': '..---',
        '3': '...--',
        '4': '....-',
        '5': '.....',
        '6': '-....',
        '7': '--...',
        '8': '---..',
        '9': '----.',
        '0': '-----'
    };
    MorseTranslator.morseToMsgRef = {
        '.-': 'a',
        '-...': 'b',
        '-.-.': 'c',
        '-..': 'd',
        '.': 'e',
        '..-.': 'f',
        '--.': 'g',
        '....': 'h',
        '..': 'i',
        '.---': 'j',
        '-.-': 'k',
        '.-..': 'l',
        '--': 'm',
        '-.': 'n',
        '---': 'o',
        '.--.': 'p',
        '--.-': 'q',
        '.-.': 'r',
        '...': 's',
        '-': 't',
        '..-': 'u',
        '...-': 'v',
        '.--': 'w',
        '-..-': 'x',
        '-.--': 'y',
        '--..': 'z',
        '.----': '1',
        '..---': '2',
        '...--': '3',
        '....-': '4',
        '.....': '5',
        '-....': '6',
        '--...': '7',
        '---..': '8',
        '----.': '9',
        '-----': '0'
    };
    return MorseTranslator;
}());
exports.MorseTranslator = MorseTranslator;
//# sourceMappingURL=morsetranslator.js.map