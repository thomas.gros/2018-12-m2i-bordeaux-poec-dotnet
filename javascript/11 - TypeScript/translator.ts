export interface Translator
{
    translate(msg: string): string;
    reversetranslate(msg: string): string;
}