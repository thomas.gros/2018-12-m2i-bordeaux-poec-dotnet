// scope = portée = qui voit la variable

var x = 42;
console.log('x: ', x);

if(true) {
    console.log('x:', x);
    
    var y = 'hello';
    console.log('y:', y);

    let z = 'world';
    console.log('z:', z);
}

console.log('y:', y);

// console.log('z:', z); // Uncaught ReferenceError: z is not defined

console.log('window.x: ', window.x);
console.log('window.y: ', window.y);
window.console.log('window: ', window);