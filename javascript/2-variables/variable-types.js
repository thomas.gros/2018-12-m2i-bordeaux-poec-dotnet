// number
let a = 1;
let b = 5.36;
console.log('a: ', a);
console.log('b: ', b);
console.log('typeof a: ', typeof a);
console.log('typeof b: ', typeof b);

// boolean
let c = true;
let d = false;
console.log('c: ', c);
console.log('d: ', d);
console.log('typeof c: ', typeof c);
console.log('typeof d: ', typeof d);

// string
let e = 'bonjour';
let f = "le monde";
let g = `ca marche aussi`; // ` = backtick.  Template Strings, depuis ES 6. 

console.log('e: ', e);
console.log('f: ', f);
console.log('g: ', g);
console.log('typeof e: ', typeof e);
console.log('typeof f: ', typeof f);
console.log('typeof g: ', typeof g);

let h = `${a}, ${b}, ${c}`;
console.log('h: ', h);

// les template strings permettent le multiligne
let i = `<div>
            <p>hello</p>
         </div>`;

console.log('i: ', i);

// null
let j = null;
console.log('j: ', j);
console.log('typeof j: ', typeof j);

// undefined
let k = undefined;
console.log('k: ', k);
console.log('typeof k: ', typeof k);

// array
let l = [1,2,3,4,5,6,7,8,9,10];
console.log('l: ', l);
console.log('typeof l: ', typeof l);

// objets
let m = {
    "nom": "Thomas",
    "prenom": "Gros",
}

console.log('m: ', m);
console.log('typeof m: ', typeof m);
