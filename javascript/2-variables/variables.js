// déclaration d'une variable
    // avant ES 6: mot clé var pour déclarer des variables
    var myvar; // déclare une variable myvar
    console.log('myvar: ', myvar);

    // en ES 6:
    let myvarES6;
    console.log('myvarES6: ', myvarES6);

    const myconstES6 = 0.01;
    console.log('myconstES6: ', myconstES6);

// affectation de valeur à une variable

    myvarES6 = new Date();
    console.log('myvarES6: ', myvarES6);

    myvarES6 = 'une autre valeur'; // réutilisation de la même variable
    console.log('myvarES6: ', myvarES6);

    myvar = true;
    console.log('myvar: ', myvar);
    
    var foo = 42; // déclare variable foo et affecte valeur 42
    var bar = 'hello'; // déclare variable bar et affecte valeur 'hello'
    
    console.log('foo: ', foo);
    console.log('bar: ', bar);