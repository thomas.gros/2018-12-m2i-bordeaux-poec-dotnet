let x = 42;

// if // else if // else

if(x <= 10) 
{
    console.log('x plus petit que 10');
} else if (x <= 20)
{
    console.log('x plus grand que 10 mais plus petit que 20');
} else
{
    console.log('x plus grand que 20');
}

// for
for (let i = 0; i < 10; i++ )
{
    console.log('i: ', i);
}

// while
let y = 50;
while (x < y)
{
    console.log('x: ', x);
    x++;
}