let pNode = document.createElement('p');
let loremNode = document.createTextNode('lorem');

// via API DOM
let classAttributeNode = document.createAttribute("class");
classAttributeNode.value = "foo";
pNode.setAttributeNode(classAttributeNode);

// via API HTML
pNode.setAttribute("bar", "42");
pNode.classList.add("another-class");

pNode.appendChild(loremNode);
document.body.appendChild(pNode);

// Exercice, en utilisant les APIs vues ci dessus:
// append dans votre body une image
// <img src="<une url uiface.co>">
let imageNode = document.createElement('img');
imageNode.setAttribute("src", "https://randomuser.me/api/portraits/men/43.jpg");
document.body.appendChild(imageNode);
