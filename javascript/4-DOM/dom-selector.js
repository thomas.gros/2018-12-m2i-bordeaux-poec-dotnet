console.log('document.querySelector ', document.querySelector('body'));
console.log('document.querySelectorAll ', document.querySelectorAll('body'));

console.log('document.querySelector ', document.querySelector('div'));
console.log('document.querySelectorAll ', document.querySelectorAll('div'));

document.querySelector('#top').innerHTML = 'hello';
document.querySelector('#bottom').innerHTML = 'world';

// Exercice
const person = {
    "firstname": "John",
    "lastname": "Doe",
    "pic": "https://randomuser.me/api/portraits/women/44.jpg"
}

// Dans le div #top
let htmlTop = `<p>${person.firstname} ${person.lastname} <img src="${person.pic}"></p>`;
document.querySelector('#top').innerHTML = htmlTop;

// Dans le div  #bottom
let htmlBottom =
    `<table>
        <tr>
             <td>${person.firstname}</td>
             <td>${person.lastname}</td>
             <td><img src="${person.pic}"></td>
         </tr>
    </table>`;
document.querySelector('#bottom').innerHTML = htmlBottom;