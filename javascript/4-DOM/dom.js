// document.body.innerText = 'hello world';
// document.body.innerText = '<p>hello</p>';

// document.body.innerHTML = '<p>hello</p>';

const data = [1,2,3,4,5,6];
for(let i = 0; i < data.length; i++) {
    // attention beaucoup de modification du DOM / synchro layout
    document.body.innerHTML += `<h${data[i]}>titre ${data[i]}</h${data[i]}>`;
}

// Exercice
// A partir de data, générer dans <body>
// <ul><li>item 1</li><li>item 2</li>...<li>item 6</li></ul>
let html = '<ul>';
for(let i = 0; i < data.length; i++) {
    html += `<li>item ${data[i]}</li>`;
}
html += '</ul>'; //<ul><li>item 1</li></ul>
document.body.innerHTML += html; // une seule modification du DOM / synchro layout

// Exercice
const article = {
    "title": "LA REVANCHE DE DI MARIA",
    "contenu": "Le huitième de finale de Ligue des champions face à Manchester United aura un parfum particulier de retrouvailles pour l'Argentin.",
    "img": "https://medias.lequipe.fr/img-photo-jpg/-gerrit-van-keulen-vi-images-presse-sports-presse-sports/1500000001096880/0:0,2000:1333-624-416-75/d8f4b.jpg"
};

console.log(article.title);
console.log(article["title"]);

let articleHTML = `
<article class="article">
    <p class="article__title">${article.title}</p>
    <img class="article__img" src="${article.img}">
    <p class="article__excerpt">${article.contenu}</p>
</article>
`;

document.body.innerHTML += articleHTML;


// autre exemple

const persons = [{"firstname": "John"}, {"firstname": "Sally"}]
// persons[0]    => {"firstname": "John"}
// persons[1]    => {"firstname": "Sally"}

let html2 = `<dl>
    <dt>firstname</dt>
    <dd>${persons[0].firstname}</dd>
    <dt>firstname</dt>
    <dd>${persons[1].firstname}</dd>
</dl>`;
document.body.innerHTML += html2;

let html3 = `<dl>`;
for(let i = 0; i < persons.length; i++) 
{
    html3 += `<dt>firstname</dt>
              <dd>${persons[i].firstname}</dd>`;
}
html3 += `</dl>`;
document.body.innerHTML += html3;

let html4 = `<table>
    <thead>
        <tr>
            <th>firstname</th>
        </tr>
    </thead>
    <tbody>`;
    for(let i = 0; i < persons.length; i++)
    {
        html4 += `<tr>
                    <td>${persons[i].firstname}</td>
                 </tr>`;
    }
html4 += `</tbody>
</table>`;

document.body.innerHTML += html4;





