function a() 
{
    console.log('coucou depuis a');
}

function b(f) 
{
    console.log('coucou depuis b');
    console.log(typeof f);
    f();
}

console.log(a); // a est passée en paramètre de log

a();
b(a);

// Exercice MAP
function plus1(x) {
    return x + 1;
}

/**
 * 
 * @param {*} tableau 
 * @param {*} f 
 * @return array. Longueur du même nombre d'éléments que le param tableau.
 * La fonction f est appliquée à tous les paramètre du param tableau.
 * 
 */
function appliqueTraitement(tableau, f) {
    // <--- coder ici --->
    let result = [];
    for(let i = 0; i < tableau.length; i++) {
        let tmp = f(tableau[i]);
        result.push(tmp);
    }
    return result;
}

const data = [1,2,3,4];
const result = appliqueTraitement(data, plus1);
console.log('result = ', result); // résultat attendu [2,3,4,5];
console.log('data = ', data); // résultat attendu [1,2,3,4];

function square(x) {
    return x * x;
}

console.log(appliqueTraitement(data, square));

// Exercice FILTER

function filtrer(tableau, f)
{
    // <!--- code ici ---->
    let result = [];
    for(let i = 0; i < tableau.length; i++)
    {
        if(f(tableau[i]))
        {
            result.push(tableau[i]);
        }
    } 
    return result;
}

function isPositive(x)
{
    return x >= 0;
}

const data2 = [5, -3, 18, 24, -6, -25];

console.log(filtrer(data2, isPositive)); // resultat attendu [5, 18, 24]
console.log(data2); // resultat attendu [5, -3, 18, 24, -6, -25]

function isEven(x)
{
    return x % 2 == 0;
}

console.log(filtrer(data2, isEven)); // resultat attendu [18, 24, -6]

// Ces fonctions sont déjà présentes en JavaScript, directement comme
// méthodes des tableaux

// appliqueTraitement <=> array.map
console.log(data.map(plus1));

// filtrer <=> array.filter
console.log(data2.filter(isPositive));

// autres méthodes intéressantes

//forEach
data.forEach(console.log);
function toHTML(x) {
    console.log(`<p>${x}</p>`);
}  
data.forEach(toHTML);

// find
console.log(data2.find(isPositive));

// sort
/**
 * si x > y return un nombre positif
 * si x == y return 0
 * si x < y return un nombre négatif
 */
function numberASC(x, y) {
    return x - y;
}
data.sort(numberASC);
console.log('data, sorted ASC', data);


function numberDESC(x, y) {
    return - (x - y);
}

data.sort(numberDESC);
console.log('data, sorted DESC', data);