// déclare une fonction via le mot clé function
function hello() 
{
    console.log('hello world');
}

// on invoque une fonction en utilisant des parenthèses
hello();


// les fonctions peuvent avoir des paramètres / arguments
function square(a) 
{
    console.log('a^2 = ', a * a);
}  

square(9);

function add(a, b) 
{
    console.log('a + b = ', a + b);
}

add(40, 2);

// les fonctions retournent une valeur

// par défaut les fonctions retournent undefined
let result = hello(); 
console.log('result = ', result); // result = undefined

function fullname(nom, prenom) {
    // mot clé return pour retourner explicitement une valeur
    return `${prenom} ${nom}`;
}
let f = fullname('Doe', 'John');
console.log('f = ', f);