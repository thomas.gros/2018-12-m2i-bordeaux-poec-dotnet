const divClickme = document.querySelector('#clickme');

function clickListener(e) {
    e.preventDefault(); // inutile ici, juste indiqué pour l'exemple
    console.log(e);
    const divDisplay = document.querySelector('#display');
    divDisplay.innerHTML = '<p>coucou</p>';
}

// ajoute un 'listener' sur le click sur divClickMe
divClickme.addEventListener('click', clickListener);


// Exercice
// lors du click sur le bouton #b1, changer la couleur du div#color
// en appliquant les classes .blue ou .yellow
// si bleu => jaune
// si jaune => bleu
// Rappel: sur un élément du DOM il y a la propriété classList
// Rappel: sur un élément du DOM il y a la méthode setAttribute et getAttribute


const buttonB1 = document.querySelector('#b1');
// console.log(buttonB1);

function changeColor(e)
{
    const divColor = document.querySelector('#color');
    
    if(divColor.getAttribute('class') == 'blue')
    {
        divColor.setAttribute('class', 'yellow');
    } else if (divColor.getAttribute('class') == 'yellow') 
    {
        divColor.setAttribute('class', 'blue');
    }
}

buttonB1.addEventListener('click', changeColor);