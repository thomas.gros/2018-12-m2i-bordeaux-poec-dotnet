// Il n'y a pas de classes à proprement parler en JavaScript 
// (sauf ES 6 classes mais c'est surtout du 'syntaxic sugar' 
// sur les constructor functions)

// Par convention les fonctions constructor commencent par une
// majuscule

function Person(firstname, lastname) {
    this.firstname = firstname;
    this.lastname = lastname;
}

// Les fonctions sont des objets
// Les fonctions constructors ont une propriété prototype
// Cette propriété est un objet, on peut donc l'augmenter de nouvelles propriétés
// La propriété prototype d'une fonction constructeur deviendra
// le prototype des objets instanciés à partir de cette fonction constructeur
Person.prototype.fullname = function() {
    return `${this.firstname} ${this.lastname}`;
}

// Par convention les fonctions constructor s'invoquent via le
// mot clé new.

// new 
// 1) construit un nouvel objet {}
// 2) invoque la fonction constructeur avec this = objet créé en 1)
// 3) le prototype de l'objet créé en 1) est la propriété prototype
// de la fonction constructor invoquée par new
// 4) retourne l'objet créé en 1), 2) et 3)

let p = new Person('John', 'Doe');
console.log(p);
console.log(typeof p);

let p2 = new Person('Sally', 'Wilson');
console.log(p2);
console.log(typeof p2);

console.log('p === p2', p === p2);

// cherché sur p, non trouvée, 
// puis cherchée sur le prototype de p, trouvée.
p.fullname();

// JavaScript est un langage orienté objet basé sur la notion de prototype