// Objets construits via syntaxe littérale { }

let o = {
    firstname: "John",
    "lastname": "Doe",
    "email address": "john.doe@example.com",
    age: 42,
    student: true,
    address: {
        street: "james watt",
        number: 4,
        city: "Merignac"
    },
    ratings: [18, 20, 14, 17, 19],
    getInfos: function () {
        return `I'm ${ this.firstname } ${ this.lastname} 
                and I'm ${ this.age} old`;
    }
}

// accéder aux propriétés des objets
console.log(o);
console.log(o.firstname);
console.log(o["firstname"]);
console.log(o["email address"]);
console.log(o.getInfos());

// on peut 'augmenter' les objets après leur création
console.log('o.language : ', o.language);
o.language = 'English';
console.log('o.language : ', o.language);