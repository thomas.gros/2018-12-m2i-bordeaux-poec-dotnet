// API XMLHttpRequest

let xhr = new XMLHttpRequest();

// {name: "Miyah Myles", email: "miyah.myles@gmail.com", position: "Office Assistant", photo: "https://images.unsplash.com/photo-1494790108377-be…&w=200&fit=max&s=707b9c33066bf8808c934c8ab394dff6"}
function personToHTML(p) {
    return `<div>
                <p>${p.name}</p>
                <p>${p.email}</p>
                <p>${p.position}</p>
                <img src="${p.photo}">
            </div>`;
}

function responseListener(e) {
    console.log('reponse est arrivée');
    const data = JSON.parse(xhr.responseText);
    console.log(data);

    let html = '';
    for(let i = 0; i< data.length; i++) {
        html +=  personToHTML(data[i]);       
    }

    document.body.innerHTML = html;
}

xhr.addEventListener('load', responseListener);

const URL = 'https://uifaces.co/api?limit=10&emotion[]=happiness';
xhr.open('GET', URL);
xhr.setRequestHeader('X-API-KEY', '177a153b3491a103eb0e62fb8bf44d');
xhr.send();
