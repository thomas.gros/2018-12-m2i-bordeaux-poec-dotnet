const API_KEY = '9a51de967ac05a292bb717047d3cbbbc'

function getPhotos() {
    const xhr = new XMLHttpRequest();

    const URL = `https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=${API_KEY}&tags=landscape&hasgeo=1&per_page=5&format=json&nojsoncallback=1`;
    xhr.open("GET", URL);
    xhr.onload = function() {
            const data = JSON.parse(xhr.responseText);
            console.log(data);
            getGeoForPhotos(data.photos.photo.map(function(p) { return p.id }));
    }
    xhr.send();
}


function getGeoForPhotos(photoIds) {
    console.log(photoIds);
    let requests=new Array(photoIds.length);
    for (let i = 0; i < photoIds.length; i++) {
        var url = `https://api.flickr.com/services/rest/?method=flickr.photos.geo.getLocation&api_key=${API_KEY}&photo_id=${photoIds[i]}&format=json&nojsoncallback=1`;
        requests[i] = new XMLHttpRequest();
        requests[i].open("GET", url);
        requests[i].onload = function() {
            var data = JSON.parse(requests[i].responseText);
            console.log(data);
        }
        requests[i].send();
    }
}

getPhotos();