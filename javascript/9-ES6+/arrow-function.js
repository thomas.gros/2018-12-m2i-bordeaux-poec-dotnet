const data = [1,2,3,4,5,6];

// syntaxe

// fonction classiques
function square(x) {
    return x * x;
}

console.log(data.map(square));

// function expression
    // affectée à une variable
let square2 = function (x) {
    return x * x;
};

console.log(square2(5));

    // directement en paramètre d'une fonction
data.map(function(x) { 
    return x * x
});

// nouvelle syntaxe pour les fonctions: arrow functions

data.map( (x) => {  return x * x } );
data.map( x => {  return x * x } );
data.map( x => x * x);

data
    .map( e => e * e )
    .filter( e => e % 2 == 0)
    .forEach(console.log);

// lexical this
// à découvrir, cf http://exploringjs.com/es6