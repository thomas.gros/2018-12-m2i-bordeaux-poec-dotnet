// Constructors functions

// function Person(firstname, lastname)
// {
//     this.firstname = firstname;
//     this.lastname = lastname
// }

// Person.prototype.fullname = function ()
// {
//     return `${this.firstname} + ${this.lastname}`;
// }

// les classes ES6, sucre syntaxique sur les fonctions constructeur + prototype
class Person {
    constructor(firstname, lastname)
    {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    fullname()
    {
        return `${this.firstname} + ${this.lastname}`;
    }
}

const p1 = new Person("John", "Doe");
const p2 = new Person("Sally", "Wilson");
console.log(p1, p2);