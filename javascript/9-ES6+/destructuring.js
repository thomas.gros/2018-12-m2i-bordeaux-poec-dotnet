// arrays
let data = [1,2,3];
let [x,y,z] = data; // destructuring dans des variabbles
console.log(x,y,z);

data = [1,2,3,4,5];
let [a, , , , b] = data;
console.log(a,b);

let[c,d, ...e] = data;
console.log(c, d, e);

data = [1,2];
let[f = 3, g = 4, h = 5] = data;
console.log(f,g,h);

// objets
let o = {
    aa: 1,
    bb: 'hello',
    cc: true
}

let {aa , bb, dd = 42} = o;
console.log(aa, bb, dd);

function doSomething({aa,bb})
{
    console.log(aa, bb);
}

doSomething(o);