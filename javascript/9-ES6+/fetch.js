// fetch est une version 'moderne' de l'API XMLHttpRequest, basée sur les promises

// function ajax(method, url) {
//     return new Promise(function(resolve, reject){
//         const xhr = new XMLHttpRequest();

//         xhr.addEventListener('load', function(e) {
//             resolve(JSON.parse(xhr.responseText));
//         });
        
//         xhr.open(method, url);
//         xhr.setRequestHeader('X-API-KEY', '177a153b3491a103eb0e62fb8bf44d');
//         xhr.send();
        
//     });
// } 

// ajax('GET', 'https://uifaces.co/api?limit=10&emotion[]=happiness')
//     .then(json =>  json.map(e => `<p>${e.name}</p><img src=${e.photo}>`)) // promise automatiquement crée
//     .then(data => {
//         let html = '';
//         data.forEach(e => html += e);
//         document.body.innerHTML += html
//     });

// Les navigateurs fournissent l'API Fetch
fetch('https://uifaces.co/api?limit=10&emotion[]=happiness')
    .then(response => response.json())
    .then(json => console.log(json));