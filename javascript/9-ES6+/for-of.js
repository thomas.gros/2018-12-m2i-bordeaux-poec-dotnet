const data = ["bonjour", "le", "monde"];

// for
for (let i = 0; i < data.length; i++) {
    console.log(data[i]);
}

// forEach
function process(e) {
    console.log(e);
}

data.forEach(process);

// ES 6 for..of
for(const e of data) {
    console.log(e);
}

for(const [i, e] of data.entries()) {
    console.log(i, e);
}