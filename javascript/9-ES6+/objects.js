const x = 42;
const y = 'hello';

let o = { 
    x, // property shorthands
    y,
    z() { // method properties
        console.log(this.x);
    } 
};

console.log(o);

const o1 = 
{
    a: 10,
    b: 20
} 

const o2 =
{
    a: 50,
    c: 30,
    d: 40,
}

let o3 = {};
// assigne dans la cible (o3) les proprietés des autres objets
Object.assign(o3, o1, o2);
console.log(o3);





