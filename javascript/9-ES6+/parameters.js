// Paramètres avec valeur par défaut

function saySomething(msg = 'hello ?')
{
    console.log(msg);
}

saySomething();
saySomething('world');

// REST Parameters

function sumPreES6() {
    // variable 'magique' arguments
    console.log(arguments);
    let sum = 0;
    for(let i = 0; i < arguments.length; i++)
    {
        sum += arguments[i];
    }
    return sum;
}

const result = sumPreES6(1,2,3,4,5,6,7,8,9,10);
console.log(result);

// Rest parameter
function sumES6(...rest)
{
    console.log(rest);
    let sum = 0;
    for(let i = 0; i < rest.length; i++)
    {
        sum += rest[i];
    }
    return sum;
}

const resultES6 = sumES6(1,2,3,4,5,6,7,8,9,10);
console.log(resultES6);