// Asynchronisme en JavaScript

// Exemple: AJAX
/**
 *  requête HTTP (AJAX) ----------------------------------> serveur HTTP
 *              
 *  <exécuter du code pendant que la requête / réponse circule sur Internet.
 * 
 *  réponse HTTP (AJAX) <--------------------------------- serveur HTTP                
 * 
 */

 // PB: JavaScript est single-threaded
 // Mais il existe un mécanisme appelé la EventLoop qui organise / planifie
 // l'exécution du code JS au sein de votre navigateur
 // Voir: https://html.spec.whatwg.org/multipage/webappapis.html#event-loop


// Objectif des promesses = eviter le callback hell;
// console.log('before');
// setTimeout(function() {
//      console.log('COUCOU - TIMER 1');

//         setTimeout(function() {
//             console.log('COUCOU - TIMER 2');

//             setTimeout(function() {
//                 console.log('COUCOU - TIMER 3');
//             }, 1000);


//         }, 2000);

// }, 1000);
// console.log('after');

// Objectif: écrire les choses comme ça
//timer1
//    .then(timer2)
//    .then(timer3);


// Promises en ES6 sont basées sur une librairie Promises/A+
// Il existe d'autres librairies mieux fournies, par exemple http://bluebirdjs.com/docs/getting-started.html

// Une promise est un objet qui a 3 états: pending, resolved, rejected.

// Pour créer une promise il faut passer une fonction 'resolver' au constructeur
// ce resolver est invoqué immédiatement !!!

// si resolve est invoquée alors la promise sera resolved avec une valeur
// si reject est invoquée alors la promise sera rejected avec une Error
function resolver(resolve, reject) {
    console.log('dans le resolver');
    console.log(resolve);
    console.log(reject);
    //resolve(42);
    reject(new Error('erreur...'));
}

function doSomething(data) {
    console.log('data: ', data);
}

function handleError(err) {
    console.log('err: ', err);
}

const p = new Promise(resolver);

p.then(doSomething) // then si la promise est resolved
 .catch(handleError); // catch si la promise est rejected

console.log("après l'instanciation de la promise");
console.log(p);


// Application: 'promisification' de l'API Timer

//timer1
//    .then(timer2)
//    .then(timer3);

function timer(time) {
    return new Promise(function(resolve, reject) {
        setTimeout(resolve, time);
    });
}

const timer1 = timer(2000);
timer1.then(function(data) {
    console.log('received: ', data);
});

timer(2000).then(function(data) { return timer(3000)})
           .then(function(data) { return timer(1000)})
           .then(function(data) { console.log('received: ', data)});