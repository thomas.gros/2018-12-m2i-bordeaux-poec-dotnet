const data = [1,2,3,4,5];
const other = [6,7, ...data];

console.log(other);

const s = 'hello world';
const chars = [...s];
console.log(chars);